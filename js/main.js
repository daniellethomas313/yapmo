var $clientID = 'dj0yJmk9S1JEdDZpRm1laUZCJmQ9WVdrOVJ6bEJWelozTTJVbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD05OA--';
var $clientSecret = '8a364b4b33227928895e92bf675a4b2f19b807ff';


$('form').submit(function(e){
	e.preventDefault();
	var $city = $('input[name="city"]').val();
	var $url = "https://query.yahooapis.com/v1/public/yql?q=select woeid from geo.places where text='" + $city +  "'&format=json&diagnostics=true&callback=getWeather";
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = $url;
	$("head").append(s);
});

function getWeather(data) {
	var $woeid = data.query.results.place[0].woeid;
	var $url = "https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid='" + $woeid + "'&format=json&diagnostics=true&callback=displayWeatherData";
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = $url;
	$("head").append(s);
}

function displayWeatherData(data) {
	var $weatherObj = data.query.results.channel;
	
	$('#city').html($weatherObj.location.city + ',' + $weatherObj.location.region);
	$('#current-temp').html($weatherObj.item.condition.temp + "F");
	$condition = $weatherObj.item.condition.text;
	checkCondition($condition);
	
	$('#current-condition').html($condition);
	$('#hi').html("Hi " + $weatherObj.item.forecast[0].high);
	$('#lo').html("Lo " + $weatherObj.item.forecast[0].low);
}


function checkCondition(condition) {
	$('#weather').removeClass();
	if (condition == "Clear" || condition == "Mostly Clear") {
		$('#weather').addClass('clear');
	}
	if (condition == "Cloudy" || condition == "Mostly Cloudy") {
		$('#weather').addClass('cloudy');
	}
	if (condition == "Partly Cloudy") {
		$('#weather').addClass('partly-cloudy');
	}
	if (condition == "Rainy") {
		$('#weather').addClass('rainy');
	}
	if (condition == "Sunny") { 
		$('#weather').addClass('sunny');
	}
}


